﻿using CollectionList2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionList2.Repository
{
    internal class ContactRepository
    {
        List<Contact> contactList;
        public ContactRepository()
        {
            contactList = new List<Contact>()
            {
                new Contact() { Name = "Abhi", Address = "Juhu", City = "Mumbai", PhoneNumber = "919191919191" },
                new Contact() { Name = "Abc", Address = "XYZ", City = "banglore", PhoneNumber = "87256272872" },

            };
        }
        public List<Contact> GetAllContact()
        {
            return (contactList);
        }
        public Contact GetContactbyName(String name)
        {
            return contactList.Find(cont => cont.Name == name);
        }
        public String AddContact(Contact contact)
        {
            var Checkexist = GetContactbyName(contact.Name);
            if(Checkexist == null)  
            {
                contactList.Add(contact);
                return $"Contact Added Successfully";

            }
            else
                return "Contact Exist"; 
        }

       

    }
           
            
    
}