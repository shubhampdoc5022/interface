﻿using FileHandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.Repository
{
    internal class UserRepository : IUserRepository
    {
        List<User> users;
        public UserRepository()
        {
           users = new List<User>();
        }
        public bool RegisterUser(User user)
        {
            users.Add(user);
            string fileName = "userDatabase.txt";
            WriteContentsToFile(user, fileName);
            return true;
        }
        public void ReadContentFromFile(String fileName)
        {
            throw new NotImplementedException();
        }
        public  bool IsUserNameExist(User user)
        {
            users.Add(user);
            string fileName = "userDatabase.txt";
            WriteContentsToFile(user, fileName);
            return true;
        }
        public void WriteContentsToFile(User user,String fileName)
        {
            using (StreamWriter  sw = new StreamWriter(fileName))
            {
                sw.Write( $"{user}");
            }


        }

    }
}








