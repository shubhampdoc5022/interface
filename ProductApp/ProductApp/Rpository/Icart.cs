﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp.Rpository
{
    internal interface Icart
    {
        void DisplayTheCart();
        void AddToCart();
        void DeleteFromCart();
        void PriceTotal();

    }
}
