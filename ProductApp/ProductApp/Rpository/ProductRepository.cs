﻿using ProductApp.Constant;
using ProductApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp.Rpository
{
    internal class ProductRepository
    {
        List<Product> products;
        List<Product> prodCart = new List<Product>();
        int tp = 0;
        string[] cityarray = new string[8] { "lucknow", "kanpur", "mumbai", "pune", "Sri nagar", "jaipur", "Udaipur", "thane", };
       
        public ProductRepository()
        {
            products = new List<Product>()
                    {
                    new Product() { ProductId = 1, ProductName = "RedmiNote4", ProductCategory = "SmartPhone", ProductPrice = 15000},
                    new Product() { ProductId = 2, ProductName = "RedmiNote3", ProductCategory = "SmartPhone", ProductPrice = 10000 },
                    new Product() { ProductId = 3, ProductName = "lenovo L440", ProductCategory = "Laptop", ProductPrice = 111000 },
                    new Product() { ProductId = 4, ProductName = "Lenovo X250", ProductCategory = "Laptop", ProductPrice = 144000 },
                    new Product() { ProductId = 5, ProductName = "Samsung G20", ProductCategory = "TV", ProductPrice = 25000 },
                    new Product() { ProductId = 6, ProductName = "LG 231", ProductCategory = "TV", ProductPrice = 28000 },
                    new Product() { ProductId = 7, ProductName = "Noise", ProductCategory = "Earbuds", ProductPrice = 3000 },
                    new Product() { ProductId = 8, ProductName = "Boat", ProductCategory = "Earbuds", ProductPrice = 5000 }
                    };
        }

        public bool city(string cityname)
        {
            bool status;
            cityname = cityname.ToLower();
            var City = cityarray.FirstOrDefault(p => p == cityname);
            if (City != null)
                status = true;
            else status = false;
            return status;
        }
        public List<Product> GetAllProducts()

        {
            return (products);
        }
        public void DisplayCart()
        {
            var cart = new List<Product>();
        }
        public void AddToCart(string name)
        {
            Product p=products.FirstOrDefault(item => item.ProductName == name);
            if(p != null)
            {
                prodCart.Add(p);
                Console.WriteLine($"Product Successfully Added");
                tp += p.ProductPrice;
            }
            else
            {
                Console.WriteLine("Nothing Added In The Cart");
            }
            MenuOption();

        }
        public void RemoveFromcart(string name)
        {
            Product p = prodCart.FirstOrDefault(item => item.ProductName == name);
            if (p != null)
            {
                prodCart.Remove(p);
                Console.WriteLine($"Product Successfully deleted");
                tp -= p.ProductPrice;
            }
            else
            {
                Console.WriteLine("Nothing deleted from The Cart");
            }
            MenuOption();
        }
        public void TotalPrice()
        {
            Console.WriteLine($"TotalPrice is:{tp}"); 
            MenuOption();

        }
       
        public void MenuOption()
        {
            Console.WriteLine("1 Filter\n2 DisplayCart\n3 AddTOCart\n4 RemoveFromCart\n5 TotalPrice\n6 Exit");
            MenuOptions menuOption = (MenuOptions) Convert.ToInt32(Console.ReadLine());
            switch (menuOption)
            {
                case MenuOptions.Filter:
                    Console.WriteLine("Enter category::");
                    string categoryName = Console.ReadLine();
                    FilterByCategory(categoryName);
                    break;
                case MenuOptions.DisplayTheCart:
                    break;
                case MenuOptions.AddToCart:
                    Console.Write("Enter product name");
                    string prodName = Console.ReadLine();
                    AddToCart(prodName);
                    break;
                case MenuOptions.RemoveFromCart:
                    Console.Write("Enter product name to be deleted::");
                    string prodName1 = Console.ReadLine();
                    RemoveFromcart(prodName1);
                    break;
                case MenuOptions.TotalPrice:
                    TotalPrice();
                    break;
                case MenuOptions.Exit:
                    break;
                default:
                    Console.WriteLine("Error");
                    break;
            }
            
        }
        
        private void FilterByCategory(string category)
        {
            Product[] pArray = products.Where(item => item.ProductCategory == category).ToArray();
            foreach (Product p in pArray)
            {
                Console.WriteLine(p);
            }
            MenuOption();
        }
    }
}





