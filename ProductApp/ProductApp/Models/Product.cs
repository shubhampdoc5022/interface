﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp.Models
{
    internal class Product
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductCategory { get; set; }
        public int ProductPrice { get; set; }
        public override string ToString()
        {
            return $"Id:{ProductId}, Name :{ProductName}, Category:{ProductCategory}, Price:{ProductPrice}";
        }
    }
}
