﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProductApp.Constant
{
    internal enum MenuOptions
    {
        Filter=1,
        DisplayTheCart,
        AddToCart,
        RemoveFromCart,
        TotalPrice,
        Exit
    }
}
