﻿using FileHandling.Exceptions;
using FileHandling.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileHandling.Repository
{
    internal class UserRepository : IUserRepository, IFile
    {
        List<User> users;

        public UserRepository()
        {
            users = new List<User>();
        }

        public bool RegisterUser(User user)
        {
            users.Add(user);
            string fileName = "userDatabase.txt";
            bool isUserNameExists = IsUserNameExists(user.Name, fileName);
            if (isUserNameExists)
            {
                return false;
            }
            else
            {
                WriteContentsToFile(user, fileName);
                return true;
            }
        }

        public bool IsUserNameExists(string userName, string fileName)
        {
            bool isUserAvalibale = false;
            using(StreamReader sr = new StreamReader(fileName))
            {
                string rowLine;
                while((rowLine = sr.ReadLine()) != null)
                {
                    string[] rowSplittedValues = rowLine.Split(",");
                    foreach(string userIndividualValue in rowSplittedValues)
                    {
                        if(userIndividualValue == userName)
                        {
                            isUserAvalibale = true;
                            throw new InvalidUserException("Username already exists.");
                            //break;
                        }
                    }
                }
            }
            return isUserAvalibale;
        }

        public List<string> ReadContentsFromFile(string fileName)
        {
            List<string> rowValues = new List<string>();
            using (StreamReader sr = new StreamReader(fileName))
            {
                string rowLine;
                while ((rowLine = sr.ReadLine()) != null)
                {
                    rowValues.Add(rowLine);
                }
                return rowValues;
            }
        }

        public void WriteContentsToFile(User user, string fileName)
        {
            using (StreamWriter sw = new StreamWriter(fileName, true))//append bool which is true
            {
                sw.WriteLine($"{user}");
            }
        }
    }
}
