﻿// See https://aka.ms/new-console-template for more information

using FileHandling.Exceptions;
using FileHandling.Model;
using FileHandling.Repository;

UserRepository userRepository = new UserRepository();
IUserRepository iuserRepository = (IUserRepository) userRepository;
User user = new User();
Console.Write("Id::");
user.Id = int.Parse(Console.ReadLine());
Console.Write("Name::");
user.Name = Console.ReadLine();
Console.Write("City::");
user.City = Console.ReadLine();

try
{
    bool isUserRegistered = iuserRepository.RegisterUser(user);
    if (isUserRegistered == true)
    {
        Console.WriteLine("Registration success");
    }
}
catch (InvalidUserException ex)
{
    Console.WriteLine(ex.Message);
}

Console.WriteLine("Reading Contents from File");
List<string> userContent = userRepository.ReadContentsFromFile("userDatabase.txt");
foreach(string fileData in userContent)
{
    Console.WriteLine(fileData);
}