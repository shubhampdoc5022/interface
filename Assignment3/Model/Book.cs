﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment3.Model
{
    internal class Book
    {
        public int BookId { get; set; }
        public string BookName { get; set; }
        public string BookAuthor { get; set; }
        public float BookPrice { get; set; }

        public Book(int bookId, string bookName, string bookAuthor, float bookPrice)
        {
            BookId = bookId;
            BookName = bookName;
            BookAuthor = bookAuthor;
            BookPrice = bookPrice;
        }

        public override string ToString()
        {
            return $"Book [ID={BookId}, Name={BookName}, Author={BookAuthor}, Price={BookPrice}]";
        }
    }
}
