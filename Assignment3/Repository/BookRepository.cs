﻿using Assignment3.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment3.Repository
{
    internal class BookRepository
    {
        public Book[] books;
        public BookRepository()
        {
            books = new Book[5]
            {
                new Book(1, "Adiyogi", "Sadhguru", 340.13f),
                new Book(2, "Twilight", "Stephenie Meyer", 200.91f),
                new Book(3, "Karma", "Sadhguru", 401),
                new Book(4, "The Shawshank Redemption", "Stephen King", 550),
                new Book(5, "Wings of Fire", "A.P.J Abdul Kalam", 650)
            };
        }

        public Book[] GetAllBooks()
        {
            return books;
        }

        public Book[] GetBooksByAuthor(string author)
        {
            Book[] booksR = Array.FindAll(books, item => item.BookAuthor == author);
            return booksR;
        }

        public Book Update(int id)
        {
            Book b = Array.Find(books, item => item.BookId == id);
            return b;
        }

        public Book[] DeleteBookById(int id)
        {
            books = books.Where(item => item.BookId != id).ToArray();
            return books;
        }

    }
}