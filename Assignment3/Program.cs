﻿// See https://aka.ms/new-console-template for more information

using Assignment3.Model;
using Assignment3.Repository;

BookRepository bookRepository = new BookRepository();
Book[] books;


books = bookRepository.GetAllBooks();
Console.WriteLine($"---- Retrieve all the books ----");
foreach (Book book in books)
{
    Console.WriteLine(book);
}


//Retrieve
string bookAuthor = "Sadhguru";
books = bookRepository.GetBooksByAuthor(bookAuthor);
Console.WriteLine($"---- Retrieve books by {bookAuthor} ----");
foreach (Book book in books)
{
    Console.WriteLine(book);
}


//Update
int id = 5;
var bk = bookRepository.Update(id);
books = bookRepository.GetAllBooks();
bk.BookName = "India 2020";
Console.WriteLine($"---- Update book by bookId={id} ----");
foreach (Book book in books)
{
    Console.WriteLine(book);
}


//Delete
id = 2;
books = bookRepository.DeleteBookById(id);
Console.WriteLine($"---- After Deletion of book having bookId={id} ----");
foreach (Book book in books)
{
    Console.WriteLine(book);
}

books = bookRepository.GetAllBooks();
Console.WriteLine($"---- Retrieve all the books ----");
foreach (Book book in books)
{
    Console.WriteLine(book);
}